public class Book{
    private String author;
    private String bookName;
    private String press;
    public String copyrightdate;

    public Book(String author, String bookName, String press, String copyrightdate) {
        this.author = author;
        this.bookName = bookName;
        this.press = press;
        this.copyrightdate = copyrightdate;
}

    public String getAuthor() {
        return author;
}

    public void setAuthor(String author) {
        this.author = author;
}

    public String getBookName() {
        return bookName;
}

    public void setBookName(String bookName) {
        this.bookName = bookName;
}

    public String getPress() {
        return press;
}

    public void setPress(String press) {
        this.press = press;
}
    
    public String getCopyRightDate() {
        return copyrightdate;
}
public void setCopyRightDate(String copyrightdate) {
this.copyrightdate = copyrightdate;
}

public String toString(){
return "Author："+author+"\n"+"-BookName："+bookName+"\n"+"-Press："+press+"\n"+"-CopyRightDate："+copyrightdate;
}
}

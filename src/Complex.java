public class Complex{
    private double RealPart;
    private double ImagePart;

        //构造函数
        public Complex(){}
        public Complex(double R,double I){
            RealPart=R;
            ImagePart=I;
        }

        //返回
        public double getRealPart(){
        return RealPart;
        }

        public double getImagePart(){
        return ImagePart;
        }

        //接收
        public void setRealPart(double realPart){
            RealPart=realPart;
        }

        public void setImagePart(double imagePart){
            ImagePart=imagePart;
        }

        //重写函数toString
        public String toString(){
            return "Complex{" +
            "RealPart=" + RealPart +
            ", ImagePart=" + ImagePart +
            '}';
        }
        public boolean equals(Object a){
            if (a==this){
                return true;
        }
            else{
                return false;
        }
        }

        // 定义公有方法:加减乘除
        public Complex ComplexAdd(Complex a){
            Complex b = new Complex(this.RealPart+a.RealPart,this.ImagePart+a.ImagePart);
            return b;
        }
        public Complex ComplexSub(Complex a){
            Complex b = new Complex(this.RealPart-a.RealPart,this.ImagePart-a.ImagePart);
            return b;
        }
        public Complex ComplexMulti(Complex a){
            Complex b = new Complex(this.RealPart*a.RealPart-this.ImagePart*a.ImagePart,this.ImagePart*a.RealPart+this.RealPart*a.ImagePart);
            return b;
        }
        public Complex ComplexDiv(Complex a){
            double scale = a.getRealPart()*a.getRealPart() + a.getImagePart()*a.getImagePart();
            Complex b = new Complex(a.getRealPart() / (a.getRealPart()*a.getRealPart() + a.getImagePart()*a.getImagePart()),
            - a.getImagePart() / (a.getRealPart()*a.getRealPart() + a.getImagePart()*a.getImagePart()));
            return this.ComplexMulti(b);
        }
}
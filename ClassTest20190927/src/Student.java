public class Student implements P.People {

    @Override
    public String eat() {
        System.out.println("People eat.");
        return "People eat.";
    }

    @Override
    public String play() {
        System.out.println("People play.");
        return "People play.";
    }

    @Override
    public String sleep() {
        System.out.println("People sleep.");
        return "People sleep.";
    }

    public static void main(String[] args) {
        Student a=new Student();
        a.eat();
        a.play();
        a.sleep();
    }
}

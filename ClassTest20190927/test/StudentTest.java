import junit.framework.TestCase;
import org.junit.Test;

public class StudentTest extends TestCase {
    Student a=new Student();
    @Test
    public void testEat() {
        assertEquals("People eat.",a.eat());
        a.eat();
    }
    @Test
    public void testPlay() {
        assertEquals("People play.",a.play());
        a.play();
    }
    @Test
    public void testSleep() {
        assertEquals("People sleep.",a.sleep());
        a.sleep();
    }
}
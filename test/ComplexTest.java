import junit.framework.TestCase;
import org.junit.Test;


public class ComplexTest extends TestCase {
    Complex a=new Complex(8.0,4.0);
    Complex b=new Complex(-4.0,2.0);
    @Test
    public void testComplexAdd() {
        assertEquals(4.0, a.ComplexAdd(b).getRealPart());
        assertEquals(6.0,a.ComplexAdd(b).getImagePart());
    }
    @Test
    public void testComplexSub() {
        assertEquals(12.0, a.ComplexSub(b).getRealPart());
        assertEquals(2.0,a.ComplexSub(b).getImagePart());
    }
    @Test
    public void testComplexMulti() {System.out.println(a.ComplexMulti(b).getImagePart());
        assertEquals(-40.0, a.ComplexMulti(b).getRealPart());
        assertEquals(0.0,a.ComplexMulti(b).getImagePart());
    }
    @Test
    public void testComplexDiv() {
        assertEquals(-1.2000000000000002, a.ComplexDiv(b).getRealPart());
        assertEquals(-1.6,a.ComplexDiv(b).getImagePart());
    }
}
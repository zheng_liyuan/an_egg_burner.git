import junit.framework.TestCase;

public class StringBufferDemoTest extends TestCase {
    StringBuffer a = new StringBuffer("qwerabcdhij");
    StringBuffer b = new StringBuffer("qwer12345678910");
    StringBuffer c = new StringBuffer("tiyioyuoiyu");

    public void testcharAt() throws Exception{
        assertEquals('q',a.charAt(0));
        assertEquals('1',b.charAt(4));
        assertEquals('o',c.charAt(4));
    }

    public void testcapacity() throws Exception{
        assertEquals(27,a.capacity());
        assertEquals(31,b.capacity());
        assertEquals(27,c.capacity());
    }

    public void testlength() throws Exception{
        assertEquals(11,a.length());
        assertEquals(15,b.length());
        assertEquals(11,c.length());
    }

    public void testindexOf() throws Exception{
        assertEquals(0,a.indexOf("qwer"));
        assertEquals(4,b.indexOf("123"));
        assertEquals(5,c.indexOf("yuo"));
    }
}
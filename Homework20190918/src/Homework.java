import java.util.*;
import java.text.*;
public class Homework{
    public static void main(String []args){
        Random gen = new Random();
        float num  = gen.nextFloat()*20-10;
        System.out.println("Random number from -10 to 10:");
        DecimalFormat fmt = new DecimalFormat("0.###");
        System.out.println(fmt.format(num));
}
}

import java.util.*;
import java.text.*;
public class PP3_5{
    public static void main(String[] args){
        double r,area,volume;

        Scanner scan = new Scanner(System.in);
        DecimalFormat fmt=new DecimalFormat("0.####");
        
        System.out.println("Please input a radius:");
        r=scan.nextDouble();
        volume=Math.PI*r*r*r*4/3;
        area=4*Math.PI*r*r;

        System.out.println("The area is:"+fmt.format(area)+"\n");
        System.out.println("The volume is:"+fmt.format(volume)+"\n");





}}
